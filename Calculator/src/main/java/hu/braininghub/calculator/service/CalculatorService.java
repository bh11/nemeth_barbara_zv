package hu.braininghub.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    private Double summation(Double firstNumber, double secondNumber){
        return firstNumber+secondNumber;
    }

    private Double substract(Double firstNumber, double secondNumber){
        return firstNumber-secondNumber;
    }

    private Double multiplication(Double firstNumber, double secondNumber){
        return firstNumber*secondNumber;
    }

    private Double division(Double firstNumber, double secondNumber){
        return firstNumber / secondNumber;

    }

    public Double getResult(Double firstNumber, Double secondNumber, Character operation) {
        Double result = 0.0;

        switch(operation){
            case '+': result = summation(firstNumber,secondNumber); break;
            case '-': result = substract(firstNumber,secondNumber); break;
            case '*': result = multiplication(firstNumber,secondNumber); break;
            case '/': result = division(firstNumber,secondNumber); break;
        }

        return result;


    }
}
