package hu.braininghub.calculator.rest;

import hu.braininghub.calculator.dto.CalculatorOperationRequest;
import hu.braininghub.calculator.dto.CalculatorOperationResponse;
import hu.braininghub.calculator.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/calculator")
public class CalculatorRestController {
    @Autowired
    private CalculatorService calculatorService;

    @RequestMapping(method = RequestMethod.POST)
    public Double resultOperation(@RequestBody CalculatorOperationRequest request){
        Double result = calculatorService.getResult(request.getFirstNumber(),request.getSecondNumber(),request.getOperation());
        CalculatorOperationResponse response = new CalculatorOperationResponse();
        response.setResult(result);
        return result;
    }
}
