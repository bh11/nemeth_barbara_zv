package hu.braininghub.calculator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class CalculatorOperationResponse implements Serializable {
    @JsonProperty("_result")
    private Double result;

    public void setResult(Double result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalculatorOperationResponse)) return false;
        CalculatorOperationResponse that = (CalculatorOperationResponse) o;
        return result.equals(that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result);
    }

    @Override
    public String toString() {
        return "CalculatorOperationResponse{" +
                "result=" + result +
                '}';
    }
}
