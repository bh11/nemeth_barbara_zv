package hu.braininghub.calculator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class CalculatorOperationRequest implements Serializable {
    @JsonProperty("_first_number")
    private Double firstNumber;
    @JsonProperty("_second_number")
    private Double secondNumber;
    @JsonProperty("_operation")
    private Character operation;

    public Double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(Double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public Double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(Double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public Character getOperation() {
        return operation;
    }

    public void setOperation(Character operation) {
        this.operation = operation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalculatorOperationRequest)) return false;
        CalculatorOperationRequest that = (CalculatorOperationRequest) o;
        return getFirstNumber().equals(that.getFirstNumber()) &&
                getSecondNumber().equals(that.getSecondNumber()) &&
                getOperation().equals(that.getOperation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstNumber(), getSecondNumber(), getOperation());
    }

    @Override
    public String toString() {
        return "CalculatorOperationRequest{" +
                "firstNumber=" + firstNumber +
                ", secondNumber=" + secondNumber +
                ", operation=" + operation +
                '}';
    }
}
