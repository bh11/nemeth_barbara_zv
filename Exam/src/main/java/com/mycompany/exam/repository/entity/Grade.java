/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exam.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.mycompany.exam.repository.ExamType;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author BlOOOD
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "grades")
public class Grade implements Serializable{
    @EqualsAndHashCode.Include
    @GeneratedValue
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
    
    @Column(name = "grade")
    private Double grade;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "exam_type")
    private ExamType examType;    
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "email")
    private Student student;
    
}
