<!DOCTYPE html>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>Student</title>
    </head>
    <body>
        <h1>Letter: </h1>
        <form method="post">
            <div class="form-group">
                <div class="col-md-12 mb-9">
                    <select class="custom-select" name="letter">
                        <option value="none">Open this select menu</option>
                        <c:forEach var="l" items="${letters}">
                            <option value="${l.letter}"><c:out value="${l.letter}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="foot">
                    <input type="submit" value="Select" />
                </div>
            </div>
        </form>


        <h1>Students: </h1>
        
        <table class="table table-striped table-dark">
            <c:forEach var = "s" items="${studentlist}">
                <tr>
                    <td>
                        <c:out value="${s.email}"/>
                    </td>
                    <td>
                        <c:out value="${s.firstName}"/>
                    </td>
                    <td>
                        <c:out value="${s.lastName}"/>
                    </td>
                </tr>
            </c:forEach>
        </table> 

    </body>
</html>
