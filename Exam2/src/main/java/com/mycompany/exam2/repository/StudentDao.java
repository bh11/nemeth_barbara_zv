/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exam2.repository;

import com.mycompany.exam2.repository.entity.Letter;
import com.mycompany.exam2.repository.entity.Student;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author BlOOOD
 */

@Singleton
@LocalBean
public class StudentDao {
    @PersistenceContext
    private EntityManager em;

    public Iterable<Student> filterByLetter(String letter) {
        return em.createQuery("SELECT s FROM Student s WHERE s.firstName LIKE :pattern", Student.class)
                .setParameter("pattern", letter + "%")
                .getResultList();
    }
}
