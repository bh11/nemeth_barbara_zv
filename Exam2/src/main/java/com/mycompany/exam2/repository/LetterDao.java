/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exam2.repository;

import com.mycompany.exam2.repository.entity.Letter;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author BlOOOD
 */

@Singleton
@LocalBean
public class LetterDao {
    @PersistenceContext
    private EntityManager em;

    public List<Letter> findAll() {
        return em.createQuery("SELECT l FROM Letter l", Letter.class).getResultList();
    }
}
