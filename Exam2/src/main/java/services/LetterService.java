/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.mycompany.exam2.repository.LetterDao;
import com.mycompany.exam2.repository.entity.Letter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import org.apache.commons.beanutils.BeanUtils;
import services.dtos.LetterDto;

/**
 *
 * @author BlOOOD
 */

@Singleton
@LocalBean
public class LetterService {
    
    @Inject
    private LetterDao dao;
    
    public List<LetterDto> getAll() {
        List<LetterDto> result = new ArrayList<>();

        for (Letter l : dao.findAll()) {
            LetterDto dto = new LetterDto();

            try {
                BeanUtils.copyProperties(dto, l);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(LetterService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return result;
    }
    
}
