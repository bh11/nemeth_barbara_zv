/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import com.mycompany.exam2.repository.StudentDao;
import com.mycompany.exam2.repository.entity.Letter;
import com.mycompany.exam2.repository.entity.Student;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import org.apache.commons.beanutils.BeanUtils;
import services.dtos.LetterDto;
import services.dtos.StudentDto;

/**
 *
 * @author BlOOOD
 */

@Singleton
@LocalBean
public class StudentService {
     @Inject
    private StudentDao dao;
    
    public List<StudentDto> getAllByLetter(String letter) {
        List<StudentDto> result = new ArrayList<>();

        for (Student s : dao.filterByLetter(letter)) {
            StudentDto dto = new StudentDto();

            try {
                BeanUtils.copyProperties(dto, s);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(StudentService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return result;
    }
}
