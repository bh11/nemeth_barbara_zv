/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import services.LetterService;
import services.StudentService;
import services.dtos.LetterDto;
import services.dtos.StudentDto;

/**
 *
 * @author BlOOOD
 */

@WebServlet(name = "StudentServlet", urlPatterns = {"/student"})
public class StudentServlet extends HttpServlet{
    @Inject
    private StudentService studentService;

    @Inject
    private LetterService letterService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<LetterDto> letters = letterService.getAll();
        request.setAttribute("letters", letters);

        request.getRequestDispatcher("/WEB-INF/student.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         
        
        String letter = request.getParameter("letter");
        List<StudentDto> studentlist = studentService.getAllByLetter(letter);
        
        request.setAttribute("studentlist", studentlist);
        request.getRequestDispatcher("/WEB-INF/student.jsp").forward(request, response);
    }
}
